'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.user_game_history, {
        foreignKey: 'user_game_id'
      }),
        this.hasMany(models.user_game_room, {
          foreignKey: 'user_game_id1'
        }),
        this.hasMany(models.user_game_room, {
          foreignKey: 'user_game_id2'
        })
    }
  }
  user_game.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game',
  });
  return user_game;
};