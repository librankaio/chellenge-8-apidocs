const express = require('express')
const router = express.Router()
//controller
const userController = require('./controllers/userController');
const gameController = require('./controllers/gameController');
//auth
const { body } = require('express-validator');
const checkToken = require('./middleware/checkToken');
const checkLogin = require('./middleware/checkLogin');


//User auth
router.get('/user/list', userController.list)
router.get('/user/list', userController.list)
router.post('/register', body('email').isEmail(), body('username').notEmpty(), body('password').notEmpty(), userController.create)
router.post('/login', body('username').notEmpty(), body('password').notEmpty(), userController.login);
router.get('/login/profile', checkToken, userController.getProfile);

//Game
router.get('/game-list', gameController.list);
router.post('/create-room', checkToken, gameController.createRoom);
router.post('/fight', checkToken, gameController.fightRoom);

module.exports = router